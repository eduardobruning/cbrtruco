/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

import java.util.ArrayList;

/**
 *
 * @author douglas giordano
 */
public class Rodada {
    private ArrayList<Ponto> pontos;
    private CartaTipo cartaRobo;
    private CartaTipo cartaHumano;
    private Resultado resultado;
    private Mao mao;

    public Rodada(){
        pontos = new ArrayList<>();
    }
    
    /**
     * @return the pontos
     */
    public ArrayList<Ponto> getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(ArrayList<Ponto> pontos) {
        this.pontos = pontos;
    }

    /**
     * @return the cartaRobo
     */
    public CartaTipo getCartaRobo() {
        return cartaRobo;
    }

    /**
     * @param cartaRobo the cartaRobo to set
     */
    public void setCartaRobo(CartaTipo cartaRobo) {
        this.cartaRobo = cartaRobo;
    }

    /**
     * @return the cartaHumano
     */
    public CartaTipo getCartaHumano() {
        return cartaHumano;
    }

    /**
     * @param cartaHumano the cartaHumano to set
     */
    public void setCartaHumano(CartaTipo cartaHumano) {
        this.cartaHumano = cartaHumano;
    }

    /**
     * @return the mao
     */
    public Mao getMao() {
        return mao;
    }

    /**
     * @param mao the mao to set
     */
    public void setMao(Mao mao) {
        this.mao = mao;
    }

    /**
     * @return the resultado
     */
    public Resultado getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(Resultado resultado) {
        this.resultado = resultado;
    }

}
