/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

/**
 *
 * @author douglas giordano
 */
public class Resultado {
    private Jogador ganhador;
    private TipoResultado resultado;

    /**
     * @return the ganhador
     */
    public Jogador getGanhador() {
        return ganhador;
    }

    /**
     * @param ganhador the ganhador to set
     */
    public void setGanhador(Jogador ganhador) {
        this.ganhador = ganhador;
    }

    /**
     * @return the resultado
     */
    public TipoResultado getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(TipoResultado resultado) {
        this.resultado = resultado;
    }
}
