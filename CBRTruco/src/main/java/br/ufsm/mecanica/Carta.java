/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

/**
 *
 * @author douglas
 */
public class Carta {

    private CartaTipo cartaTipo;
    private boolean jogada;

    /**
     * @return the cartaTipo
     */
    public CartaTipo getCartaTipo() {
        return cartaTipo;
    }

    /**
     * @param cartaTipo the cartaTipo to set
     */
    public void setCartaTipo(CartaTipo cartaTipo) {
        this.cartaTipo = cartaTipo;
    }

    /**
     * @return the jogada
     */
    public boolean isJogada() {
        return jogada;
    }

    /**
     * @param jogada the jogada to set
     */
    public void setJogada(boolean jogada) {
        this.jogada = jogada;
    }

    public Integer getPeso() {
        return cartaTipo.getPeso();
    }

    public String getNome() {
        return cartaTipo.getNome();
    }

    public String getDescricaoCurta() {
        return cartaTipo.getDescricaoCurta();
    }
}
