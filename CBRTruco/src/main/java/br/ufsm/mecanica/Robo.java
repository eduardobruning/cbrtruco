/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

import br.ufsm.cbrtruco.cbr.CBRApplication;
import br.ufsm.cbrtruco.representation.CaseDescription;
import br.ufsm.trucocbr.view.TrucoView;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;

/**
 *
 * @author douglas
 */
public class Robo extends Jogador {

    public boolean pedirTruco(Jogo truco) {
        boolean pedir = false;
        if (this.pediu != Origem.TRUCO && this.pediu != Origem.RETRUCO && this.pediu != Origem.VALEQUATRO) {
            CBRApplication cbrApp = CBRApplication.getCBR();
            CaseDescription a = truco.getCaseDescription();
            switch (truco.getStatus().getRodadaAtualIndex()) {
                case 1:
                    a.setQuandoJogadorChamouTruco("Primeira Rodada");
                    break;
                case 2:
                    a.setQuandoJogadorChamouTruco("Segunda Rodada");
                    break;
                case 3:
                    a.setQuandoJogadorChamouTruco("Terceira Rodada");
                    break;
            }
            a.setJogadorChamouTruco("Jogador Robo");
            a.setJogadorGanhouTruco("Jogador Robo");
            CBRQuery query = new CBRQuery();
            query.setDescription(a);
            try {
                cbrApp.cycle(query);
            } catch (ExecutionException ex) {
                Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
            }
            Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
            for (RetrievalResult r : result) {
                pedir = ((CaseDescription) r.get_case().getDescription()).getJogadorChamouTruco().equals("Jogador Robo");
                break;
            }
            if (pedir) {
                this.pediu = Origem.TRUCO;
            }
            return pedir;

        } else {
            return false;
        }
    }

    public boolean pedirReTruco(Jogo truco) {
        boolean pedir = false;
        if (this.pediu != Origem.TRUCO && this.pediu != Origem.VALEQUATRO) {
            CBRApplication cbrApp = CBRApplication.getCBR();
            CaseDescription a = truco.getCaseDescription();
            switch (truco.getStatus().getRodadaAtualIndex()) {
                case 1:
                    a.setQuandoJogadorChamouTruco("Primeira Rodada");
                    break;
                case 2:
                    a.setQuandoJogadorChamouTruco("Segunda Rodada");
                    break;
                case 3:
                    a.setQuandoJogadorChamouTruco("Terceira Rodada");
                    break;
            }
            a.setJogadorChamouTruco("Jogador Robo");
            a.setJogadorGanhouTruco("Jogador Robo");
            CBRQuery query = new CBRQuery();
            query.setDescription(a);
            try {
                cbrApp.cycle(query);
            } catch (ExecutionException ex) {
                Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
            }
            Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
            for (RetrievalResult r : result) {
                pedir = ((CaseDescription) r.get_case().getDescription()).getJogadorChamouTruco().equals("Jogador Robo");
                break;
            }
            if (pedir) {
                this.pediu = Origem.RETRUCO;
            }
            return pedir;

        } else {
            return false;
        }
    }

    public boolean pedirValeQuatro(Jogo truco) {
        boolean pedir = false;
        if (this.pediu == Origem.TRUCO && this.pediu != Origem.RETRUCO) {
            CBRApplication cbrApp = CBRApplication.getCBR();
            CaseDescription a = truco.getCaseDescription();
            switch (truco.getStatus().getRodadaAtualIndex()) {
                case 1:
                    a.setQuandoJogadorChamouTruco("Primeira Rodada");
                    break;
                case 2:
                    a.setQuandoJogadorChamouTruco("Segunda Rodada");
                    break;
                case 3:
                    a.setQuandoJogadorChamouTruco("Terceira Rodada");
                    break;
            }
            a.setJogadorChamouTruco("Jogador Robo");
            a.setJogadorGanhouTruco("Jogador Robo");
            CBRQuery query = new CBRQuery();
            query.setDescription(a);
            try {
                cbrApp.cycle(query);
            } catch (ExecutionException ex) {
                Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
            }
            Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
            for (RetrievalResult r : result) {
                pedir = ((CaseDescription) r.get_case().getDescription()).getJogadorGanhouTruco().equals("Jogador Robo");
                break;
            }
            if (pedir) {
                this.pediu = Origem.VALEQUATRO;
            }
            return pedir;

        } else {
            return false;
        }
    }

    public boolean aceitarTruco(Jogo truco) {
        boolean aceitou = false;
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        switch (truco.getStatus().getRodadaAtualIndex()) {
            case 1:
                a.setQuandoJogadorChamouTruco("Primeira Rodada");
                break;
            case 2:
                a.setQuandoJogadorChamouTruco("Segunda Rodada");
                break;
            case 3:
                a.setQuandoJogadorChamouTruco("Terceira Rodada");
                break;
        }
        a.setJogadorChamouTruco("Jogador Humano");
        a.setJogadorNaoQuisTruco("NULL");
        a.setJogadorGanhouTruco("Jogador Robo");
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.aceitarRejeitarTruco(query, truco.getStatus().getRodadaAtualIndex());
        for (RetrievalResult r : result) {
            aceitou = ((CaseDescription) r.get_case().getDescription()).getJogadorGanhouTruco().equals("Jogador Robo");
            break;
        }
        return aceitou;
    }

    public boolean aceitarReTruco(Jogo truco) {
        boolean aceitou = false;
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        switch (truco.getStatus().getRodadaAtualIndex()) {
            case 1:
                a.setQuandoJogadorChamouRetruco("Primeira Rodada");
                break;
            case 2:
                a.setQuandoJogadorChamouRetruco("Segunda Rodada");
                break;
            case 3:
                a.setQuandoJogadorChamouRetruco("Terceira Rodada");
                break;
        }
        a.setJogadorChamouRetruco("Jogador Humano");
        a.setJogadorNaoQuisTruco("NULL");
        a.setJogadorGanhouTruco("Jogador Robo");
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.aceitarReTruco(query, truco.getStatus().getRodadaAtualIndex());
        for (RetrievalResult r : result) {
            aceitou = ((CaseDescription) r.get_case().getDescription()).getJogadorGanhouTruco().equals("Jogador Robo");
            break;
        }
        return aceitou;
    }

    public boolean aceitarValeQuatro(Jogo truco) {
        boolean aceitou = false;
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        switch (truco.getStatus().getRodadaAtualIndex()) {
            case 1:
                a.setQuandoJogadorChamouValeQuatro("Primeira Rodada");
                break;
            case 2:
                a.setQuandoJogadorChamouValeQuatro("Segunda Rodada");
                break;
            case 3:
                a.setQuandoJogadorChamouValeQuatro("Terceira Rodada");
                break;
        }
        a.setJogadorChamouValeQuatro("Jogador Humano");
        a.setJogadorNaoQuisTruco("NULL");
        a.setJogadorGanhouTruco("Jogador Robo");
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.aceitarValeQuatro(query, truco.getStatus().getRodadaAtualIndex());
        for (RetrievalResult r : result) {
            aceitou = ((CaseDescription) r.get_case().getDescription()).getJogadorGanhouTruco().equals("Jogador Robo");
            break;
        }
        return aceitou;
    }

    public void jogarCarta1(Jogo truco) {
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
        for (RetrievalResult r : result) {
            Integer carta = ((CaseDescription) r.get_case().getDescription()).getPrimeiraCartaRobo();
            Carta cartaRobo = truco.getRobo().getCarta(carta);
            cartaRobo.setJogada(true);
            truco.getStatus().getRodadaAtual().setCartaRobo(cartaRobo.getCartaTipo());
            break;
        }
    }

    public void jogarCarta2(Jogo truco) {
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(Robo.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
        for (RetrievalResult r : result) {
            Integer carta = ((CaseDescription) r.get_case().getDescription()).getPrimeiraCartaRobo();
            Carta cartaRobo = truco.getRobo().getCarta(carta);
            cartaRobo.setJogada(true);
            truco.getStatus().getRodadaAtual().setCartaRobo(cartaRobo.getCartaTipo());
            break;
        }
    }

    public void jogarCarta3(Jogo truco) {
        CBRApplication cbrApp = CBRApplication.getCBR();
        CaseDescription a = truco.getCaseDescription();
        CBRQuery query = new CBRQuery();
        query.setDescription(a);
        try {
            cbrApp.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(TrucoView.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collection<RetrievalResult> result = cbrApp.consultaCartasPrimeiraRodada(query);
        for (RetrievalResult r : result) {
            Integer carta = ((CaseDescription) r.get_case().getDescription()).getPrimeiraCartaRobo();

            Carta cartaRobo = truco.getRobo().getCarta(carta);
            if (cartaRobo == null) {
                for (Carta c : getCartas()) {
                    if (!c.isJogada()) {
                        truco.getStatus().getRodadaAtual().setCartaRobo(c.getCartaTipo());
                        c.setJogada(true);
                        return;
                    }
                }
            }
            cartaRobo.setJogada(true);
            truco.getStatus().getRodadaAtual().setCartaRobo(cartaRobo.getCartaTipo());
            break;
        }
    }
}
