/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

import br.ufsm.cbrtruco.representation.CaseDescription;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author douglas giordano
 */
public class Jogo {

    private Robo robo;
    private Jogador humano;
    private Status status;
    private ArrayList<Mao> maos;

    public Jogo() {
        maos = new ArrayList<>();
    }

    public void iniciarJogo() {
        setRobo(new Robo());
        setHumano(new Jogador());

        Mao mao = new Mao();
        mao.setJogadorMao(this.getRobo());
        mao.setJogadorPe(this.getHumano());

        Rodada rodada = new Rodada();
        rodada.setMao(mao);
        rodada.setResultado(new Resultado());
        mao.addRodada(rodada);
        getMaos().add(mao);

        this.setStatus(new Status());
        this.getStatus().setRodadaAtual(rodada);
        this.getStatus().setJogadorAtual(getRobo());
        this.getStatus().setMao(mao);
        darCartas();
    }

    private void darCartas() {
        ArrayList<Carta> cartasSorteadas = this.sortearCartas();
        getRobo().setCartas(new ArrayList<>());
        getRobo().getCartas().add(cartasSorteadas.get(0));
        getRobo().getCartas().add(cartasSorteadas.get(2));
        getRobo().getCartas().add(cartasSorteadas.get(4));
        getHumano().setCartas(new ArrayList<>());
        getHumano().getCartas().add(cartasSorteadas.get(1));
        getHumano().getCartas().add(cartasSorteadas.get(3));
        getHumano().getCartas().add(cartasSorteadas.get(5));
        humano.ordenarCartas();
        robo.ordenarCartas();
    }

    private ArrayList<Carta> sortearCartas() {
        Random random = new Random();
        ArrayList<Carta> cartas = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            CartaTipo csorteada = null;
            Carta carta = new Carta();
            do {
                csorteada = CartaTipo.values()[random.nextInt(CartaTipo.values().length)];
                carta.setCartaTipo(csorteada);
            } while (cartas.contains(carta));
            csorteada.setDisponível(true);
            cartas.add(carta);
        }
        return cartas;
    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the maos
     */
    public ArrayList<Mao> getMaos() {
        return maos;
    }

    /**
     * @param maos the maos to set
     */
    public void setMaos(ArrayList<Mao> maos) {
        this.maos = maos;
    }

    /**
     * @return the robo
     */
    public Robo getRobo() {
        return robo;
    }

    /**
     * @param robo the robo to set
     */
    public void setRobo(Robo robo) {
        this.robo = robo;
    }

    /**
     * @return the humano
     */
    public Jogador getHumano() {
        return humano;
    }

    /**
     * @param humano the humano to set
     */
    public void setHumano(Jogador humano) {
        this.humano = humano;
    }

    public CaseDescription getCaseDescription() {
        CaseDescription a = new CaseDescription();
        Rodada rodada1 = getStatus().getMao().getRodada1();
        if (rodada1 != null) {
            getRodada(rodada1, a);
            CartaTipo cartaHumano = rodada1.getCartaHumano();
            CartaTipo cartaRobo = rodada1.getCartaRobo();
            if (cartaHumano != null) {
                a.setPrimeiraCartaHumano(cartaHumano.getPeso());
            }
            if (cartaRobo != null) {
                a.setPrimeiraCartaRobo(cartaRobo.getPeso());
            }
            setJogadorGanhadorRodada(rodada1, a);
        }

        Rodada rodada2 = getStatus().getMao().getRodada2();
        if (rodada2 != null) {
            getRodada(rodada2, a);
            CartaTipo cartaHumano = rodada2.getCartaHumano();
            CartaTipo cartaRobo = rodada2.getCartaRobo();
            if (cartaHumano != null) {
                a.setSegundaCartaHumano(cartaHumano.getPeso());
            }
            if (cartaRobo != null) {
                a.setSegundaCartaRobo(cartaRobo.getPeso());
            }
            setJogadorGanhadorRodada(rodada2, a);
        }

        Rodada rodada3 = getStatus().getMao().getRodada2();
        if (rodada3 != null) {
            getRodada(rodada3, a);
            CartaTipo cartaHumano = rodada1.getCartaHumano();
            CartaTipo cartaRobo = rodada1.getCartaRobo();
            if (cartaHumano != null) {
                a.setTerceiraCartaHumano(cartaHumano.getPeso());
            }
            if (cartaRobo != null) {
                a.setTerceiraCartaRobo(cartaRobo.getPeso());
            }
            setJogadorGanhadorRodada(rodada3, a);
        }

        a.setCartaAltaRobo(getRobo().getCarta1().getPeso());
        a.setCartaMediaRobo(getRobo().getCarta2().getPeso());
        a.setCartaBaixaRobo(getRobo().getCarta3().getPeso());
        if (getStatus().getMao().getJogadorMao() == robo) {
            a.setJogadorMao("Jogador Robo");
        } else {
            a.setJogadorMao("Jogador Humano");
        }
        return a;
    }

    private void getRodada(Rodada rodada, CaseDescription a) {
        if (rodada.getResultado().getGanhador() == robo) {
            a.setGanhadorPrimeiraRodada("Jogador Robo");
        } else {
            a.setGanhadorPrimeiraRodada("Jogador Humano");
        }
    }

    private void setJogadorGanhadorRodada(Rodada rodada, CaseDescription a) {
        if (rodada.getResultado().getGanhador() == this.robo) {
            a.setGanhadorPrimeiraRodada("Jogador Robo");
        } else if (rodada.getResultado().getGanhador() == this.humano) {
            a.setGanhadorPrimeiraRodada("Jogador Humano");
        }
    }

    private void getGanhador(Rodada rodada, CaseDescription a) {
        CartaTipo cartaHumano = rodada.getCartaHumano();
        CartaTipo cartaRobo = rodada.getCartaRobo();
        if (cartaHumano != null) {
            a.setPrimeiraCartaHumano(cartaHumano.getPeso());
        }
        if (cartaRobo != null) {
            a.setPrimeiraCartaRobo(cartaRobo.getPeso());
        }
        if (rodada.getResultado().getGanhador() == robo) {
            a.setGanhadorPrimeiraRodada("Jogador Robo");
        } else {
            a.setGanhadorPrimeiraRodada("Jogador Humano");
        }
    }

    public void verificaRodada() {
        Rodada rodada = getStatus().getRodadaAtual();
        CartaTipo cartaHumano = rodada.getCartaHumano();
        CartaTipo cartaRobo = rodada.getCartaRobo();
        if (cartaHumano.getPeso() > cartaRobo.getPeso()) {
            rodada.getResultado().setGanhador(humano);
            rodada.getResultado().setResultado(TipoResultado.GANHADOR);
        } else if (cartaHumano.getPeso() < cartaRobo.getPeso()) {
            rodada.getResultado().setGanhador(robo);
            rodada.getResultado().setResultado(TipoResultado.GANHADOR);
        } else {
            rodada.getResultado().setResultado(TipoResultado.EMPATE);
        }
        Mao mao = getStatus().getMao();
        if (mao.getRodadas().size() != 3) {
            Rodada novaRodada = new Rodada();
            novaRodada.setMao(mao);
            novaRodada.setResultado(new Resultado());
            mao.addRodada(novaRodada);
            this.status.setRodadaAtual(novaRodada);
        }
    }

    public void verificaMao() {
        Rodada rodada;
        Mao mao = getStatus().getMao();
        Mao novamao = new Mao();
        rodada = new Rodada();
        rodada.setMao(novamao);
        rodada.setResultado(new Resultado());
        novamao.addRodada(rodada);
        this.humano.pediu = null;
        this.robo.pediu = null;
        this.getStatus().setRodadaAtual(rodada);

        this.getStatus().setMao(novamao);
        if (mao.getJogadorMao() == robo) {
            novamao.setJogadorMao(humano);
            novamao.setJogadorPe(robo);
            this.getStatus().setJogadorAtual(humano);
        } else {
            novamao.setJogadorMao(robo);
            novamao.setJogadorPe(humano);
            this.getStatus().setJogadorAtual(robo);
        }
        this.maos.add(mao);
        darCartas();
    }
    
    public Jogador getOutroJogador(Jogador jogador){
        if(jogador == this.robo){
            return this.humano;
        } else {
            return this.robo;
        }
    }
}
