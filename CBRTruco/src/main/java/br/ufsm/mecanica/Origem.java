/*
 */
package br.ufsm.mecanica;

/**
 *
 * @author douglas
 */
public enum Origem {
    MAO(1), INVIDO(2), REALINVIDO(5), FALTAINVIDO(0), TRUCO(2), RETRUCO(3), VALEQUATRO(4), FLOR(3), CONTRAFLOR(6);
    
    private int quantidade;
    Origem(int quantidade){
        this.quantidade = quantidade;
    }
    
    public int getQuantidade(){
        return this.quantidade;
    }
}
