/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

/**
 *
 * @author douglas giordano
 */
public class Status {

    private Rodada rodadaAtual;
    private Jogador jogadorAtual;
    private Mao mao;

    /**
     * @return the rodadaAtual
     */
    public Rodada getRodadaAtual() {
        return rodadaAtual;
    }

    /**
     * @param rodadaAtual the rodadaAtual to set
     */
    public void setRodadaAtual(Rodada rodadaAtual) {
        this.rodadaAtual = rodadaAtual;
    }

    /**
     * @return the jogadorAtual
     */
    public Jogador getJogadorAtual() {
        return jogadorAtual;
    }

    /**
     * @param jogadorAtual the jogadorAtual to set
     */
    public void setJogadorAtual(Jogador jogadorAtual) {
        this.jogadorAtual = jogadorAtual;
    }

    /**
     * @return the mao
     */
    public Mao getMao() {
        return mao;
    }

    /**
     * @param mao the mao to set
     */
    public void setMao(Mao mao) {
        this.mao = mao;
    }

    public int getRodadaAtualIndex() {
        return (mao.getRodadas().indexOf(rodadaAtual)+1);
    }
}
