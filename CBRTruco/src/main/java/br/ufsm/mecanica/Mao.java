/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

import java.util.ArrayList;

/**
 *
 * @author douglas giordano
 */
public class Mao {

    private Jogador jogadorMao;
    private Jogador jogadorPe;
    private ArrayList<Rodada> rodadas;
    protected Jogador ganhador;

    public Mao() {
        this.rodadas = new ArrayList<>();
    }

    /**
     * @return the jogadorMao
     */
    public Jogador getJogadorMao() {
        return jogadorMao;
    }

    /**
     * @param jogadorMao the jogadorMao to set
     */
    public void setJogadorMao(Jogador jogadorMao) {
        this.jogadorMao = jogadorMao;
    }

    /**
     * @return the jogadorPe
     */
    public Jogador getJogadorPe() {
        return jogadorPe;
    }

    /**
     * @param jogadorPe the jogadorPe to set
     */
    public void setJogadorPe(Jogador jogadorPe) {
        this.jogadorPe = jogadorPe;
    }

    /**
     * @return the rodadas
     */
    public ArrayList<Rodada> getRodadas() {
        return rodadas;
    }

    public void addRodada(Rodada rodada) {
        this.rodadas.add(rodada);
    }

    public Rodada getRodada1() {
        return this.getRodadas().get(0);
    }

    public Rodada getRodada2() {
        if (this.getRodadas().size() <= 1) {
            return null;
        }
        return this.getRodadas().get(1);
    }

    public Rodada getRodada3() {
        if (this.getRodadas().size() <= 2) {
            return null;
        }
        return this.getRodadas().get(2);
    }

    public int calcularPontos() {
        int total = 0;
        int pontostruco = 0;
        for (Rodada r : rodadas) {
            for (Ponto p : r.getPontos()) {
                if (p.getOrigem() == Origem.TRUCO
                        || p.getOrigem() == Origem.RETRUCO
                        || p.getOrigem() == Origem.VALEQUATRO) {
                    pontostruco = p.getQuantidade();
                } else {
                    total = total + p.getQuantidade();
                }
            }
        }
        total = total + pontostruco;
        if (total == 0) {
            return 1;
        } else {
            return total;
        }
    }

    public Jogador getGanhadorMao() {
        int pontos = calcularPontos();
        if (this.ganhador != null) {

            this.ganhador.setPontos(ganhador.getPontos()+pontos);
            return this.ganhador;
        }
        int rodadasGanhasJogador1 = 0;
        int rodadasGanhasJogador2 = 0;
        int rodadasEmpate = 0;
        Jogador ganhadorMao = null;
        for (Rodada rodada : getRodadas()) {
            Jogador ganhador = rodada.getResultado().getGanhador();
            if (ganhador != null) {
                if (ganhador == this.jogadorMao) {
                    rodadasGanhasJogador1++;
                } else if (ganhador == this.jogadorPe) {
                    rodadasGanhasJogador2++;
                }
            } else if (rodada.getResultado().getResultado() == TipoResultado.EMPATE) {
                rodadasEmpate++;
            }
        }
        if (rodadasEmpate == 2) {
            ganhadorMao = jogadorMao;
        }
        if (rodadasEmpate == 1) {
            if (rodadasGanhasJogador1 >= 1) {
                ganhadorMao = this.jogadorMao;
            } else if (rodadasGanhasJogador2 >= 1) {
                ganhadorMao = this.jogadorPe;
            }
        }
        if (rodadasGanhasJogador1 >= 2) {
            ganhadorMao = this.jogadorMao;
        } else if (rodadasGanhasJogador2 >= 2) {
            ganhadorMao = this.jogadorPe;
        }
        if (ganhadorMao != null) {
            ganhadorMao.setPontos(ganhadorMao.getPontos() + pontos);
        }
        this.ganhador = ganhadorMao;
        return ganhadorMao;
    }

    /**
     * @return the ganhador
     */
    public Jogador getGanhador() {
        return ganhador;
    }

    /**
     * @param ganhador the ganhador to set
     */
    public void setGanhador(Jogador ganhador) {
        this.ganhador = ganhador;
    }
}
