/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

/**
 *
 * @author douglas
 */
public enum CartaTipo {
    _1E("Ás de Espadas", "1E", 68, false),
    _1B("Ás de Bastos", "1B", 64, false),
    _7E("7 de Espadas", "7E", 60, false),
    _7O("7 de Ouro", "7O", 56, false),
    _3E("3 de Espadas", "3E", 48, false),
    _3B("3 de Bastos", "3B", 48, false),
    _3O("3 de Ouro", "3O", 48, false),
    _3C("3 de Copas", "3C", 48, false),
    _2E("2 de Espadas", "2E", 44, false),
    _2B("2 de Bastos", "2B", 44, false),
    _2O("2 de Ouro", "2O", 44, false),
    _2C("2 de Copas", "2C", 44, false),
    _1O("1 de Ouro", "1O", 40, false),
    _1C("Ás de Copas", "1O", 40, false),
    _12E("12 de Espadas", "12E", 32, false),
    _12B("12 de Bastos", "12B", 32, false),
    _12O("12 de Ouro", "12O", 32, false),
    _12C("12 de Copas", "12C", 32, false),
    _11E("11 de Espadas", "11E", 28, false),
    _11B("11 de Bastos", "11B", 28, false),
    _11O("11 de Ouro", "11O", 28, false),
    _11C("11 de Copas", "11C", 28, false),
    _10E("10 de Espadas", "10E", 24, false),
    _10B("10 de Bastos", "10B", 24, false),
    _10O("10 de Ouro", "10O", 24, false),
    _10C("10 de Copas", "10C", 24, false),
    _7B("7 de Bastos", "7B", 16, false),
    _7C("7 de Copas", "7C", 16, false),
    _6E("6 de Espadas", "6E", 12, false),
    _6B("6 de Bastos", "6B", 12, false),
    _6O("6 de Ouro", "6O", 12, false),
    _6C("6 de Copas", "6C", 12, false),
    _5E("5 de Espadas", "5E", 8, false),
    _5B("5 de Bastos", "5B", 8, false),
    _5O("5 de Ouro", "5O", 8, false),
    _5C("5 de Copas", "5C", 8, false),
    _4E("4 de Espadas", "4E", 4, false),
    _4B("4 de Bastos", "4B", 4, false),
    _4O("4 de Ouro", "4O", 4, false),
    _4C("4 de Copas", "4C", 4, false);

    private String nome;
    private String descricaoCurta;
    private Integer peso;
    private boolean disponível;

    CartaTipo(String nome, String descricaoCurta, Integer peso, boolean disponivel) {
        this.nome = nome;
        this.descricaoCurta = descricaoCurta;
        this.peso = peso;
        this.disponível = disponivel;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the descricaoCurta
     */
    public String getDescricaoCurta() {
        return descricaoCurta;
    }

    /**
     * @param descricaoCurta the descricaoCurta to set
     */
    public void setDescricaoCurta(String descricaoCurta) {
        this.descricaoCurta = descricaoCurta;
    }

    /**
     * @return the peso
     */
    public Integer getPeso() {
        return peso;
    }

    /**
     * @param peso the peso to set
     */
    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public static CartaTipo getCarta(String descricaoCurta) {
        for (CartaTipo carta : CartaTipo.values()) {
            if (carta.getDescricaoCurta().equalsIgnoreCase(descricaoCurta)) {
                return carta;
            }
        }
        return null;
    }



    /**
     * @return the disponível
     */
    public boolean isDisponível() {
        return disponível;
    }

    /**
     * @param disponível the disponível to set
     */
    public void setDisponível(boolean disponível) {
        this.disponível = disponível;
    }
}
