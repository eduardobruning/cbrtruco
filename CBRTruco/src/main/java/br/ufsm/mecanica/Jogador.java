/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.mecanica;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author douglas
 */
public class Jogador {

    private int pontos;
    private ArrayList<Carta> cartas;
    protected Origem pediu;

    public Jogador() {
        this.cartas = new ArrayList<>();
    }

    /**
     * @return the cartas
     */
    public ArrayList<Carta> getCartas() {
        return cartas;
    }

    /**
     * @param cartas the cartas to set
     */
    public void setCartas(ArrayList<Carta> cartas) {
        this.cartas = cartas;
    }

    public Carta getCarta1() {
        return this.cartas.get(0);
    }

    public Carta getCarta2() {
        return this.cartas.get(1);
    }

    public Carta getCarta3() {
        return this.cartas.get(2);
    }

    public void ordenarCartas() {
        Collections.sort(this.cartas, (o1, o2) -> {
            if (o1.getCartaTipo().getPeso() > o2.getCartaTipo().getPeso()) {
                return 1;
            }
            if (o1.getCartaTipo().getPeso() < o2.getCartaTipo().getPeso()) {
                return -1;
            }
            return 0;
        });
    }

    public Carta getCarta(Integer peso) {
        ArrayList<Carta> cartas = new ArrayList<>();
        for (Carta car : this.cartas) {
            if (!car.isJogada()) {
                cartas.add(car);
            }
        }
        if (cartas.size() == 1) {
            return cartas.get(0);
        }
        int distancias[] = new int[cartas.size()];
        for (int i = 0; i < cartas.size(); i++) {
            if (cartas.get(i).getPeso() == peso) {
                return cartas.get(i);
            } else {
                distancias[i] = cartas.get(i).getPeso() - peso;
            }
        }
        Carta menorDistancia = null;
        for (int i = 0; i < distancias.length - 1; i++) {
            if (distancias[i] < distancias[i + 1]) {
                menorDistancia = cartas.get(i);
            } else {
                menorDistancia = cartas.get(i + 1);
            }
        }
        return menorDistancia;
    }

    /**
     * @return the pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * @param pontos the pontos to set
     */
    public void setPontos(int pontos) {
        this.pontos = pontos;
    }

    public void aceitarTruco(Rodada rodada) {
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.TRUCO);
        ponto.setQuantidade(2);
        rodada.getPontos().add(ponto);
    }

    public void pedirTruco(Rodada rodada) {
        this.pediu = Origem.TRUCO;
    }

    public void pedirReTruco(Rodada rodada) {
        this.pediu = Origem.RETRUCO;
    }

    public void pedirValeQuatro(Rodada rodada) {
        this.pediu = Origem.VALEQUATRO;
    }

    public void recursarTruco(Jogo jogo) {
        Rodada rodada = jogo.getStatus().getRodadaAtual();
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.TRUCO);
        ponto.setQuantidade(1);
        rodada.getPontos().add(ponto);
        jogo.getStatus().getMao().ganhador = jogo.getOutroJogador(this);
    }

    public void aceitarReTruco(Rodada rodada) {
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.RETRUCO);
        ponto.setQuantidade(Origem.RETRUCO.getQuantidade());
        rodada.getPontos().add(ponto);
    }

    public void recursarReTruco(Jogo jogo) {
        Rodada rodada = jogo.getStatus().getRodadaAtual();
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.RETRUCO);
        ponto.setQuantidade(2);
        rodada.getPontos().add(ponto);
        jogo.getStatus().getMao().ganhador = jogo.getOutroJogador(this);
    }

    public void aceitarValeQuatro(Rodada rodada) {
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.VALEQUATRO);
        ponto.setQuantidade(Origem.VALEQUATRO.getQuantidade());
        rodada.getPontos().add(ponto);
    }

    public void recursarValeQuatro(Jogo jogo) {
        Rodada rodada = jogo.getStatus().getRodadaAtual();
        Ponto ponto = new Ponto();
        ponto.setOrigem(Origem.VALEQUATRO);
        ponto.setQuantidade(3);
        rodada.getPontos().add(ponto);
        jogo.getStatus().getMao().ganhador = jogo.getOutroJogador(this);
    }

    /**
     * @return the pediu
     */
    public Origem getPediu() {
        return pediu;
    }

    /**
     * @param pediu the pediu to set
     */
    public void setPediu(Origem pediu) {
        this.pediu = pediu;
    }
}
