/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.cbrtruco.cbr;

/**
 *
 * @author Eduardo Bruning
 */
import jcolibri.cbraplications.StandardCBRApplication;
import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRCaseBase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.cbrcore.Connector;
import jcolibri.exception.ExecutionException;
import jcolibri.exception.InitializingException;
import jcolibri.cbrcore.Attribute;
import javax.annotation.Generated;
import br.ufsm.cbrtruco.representation.CaseDescription;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import jcolibri.method.retrieve.NNretrieval.similarity.global.Average;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Equal;
import jcolibri.method.retrieve.NNretrieval.NNConfig;
import jcolibri.method.retrieve.RetrievalResult;
import jcolibri.method.retrieve.NNretrieval.NNScoringMethod;
import jcolibri.method.retrieve.NNretrieval.similarity.local.Interval;
import jcolibri.method.retrieve.selection.SelectCases;

public class CBRApplication implements StandardCBRApplication {

    private static CBRApplication CBR;

    @Generated(value = {"ColibriStudio"})
    Connector connector;

    @Generated(value = {"ColibriStudio"})
    CBRCaseBase casebase;

    public static CBRApplication getCBR() {
        if (CBR == null) {
            CBR = new CBRApplication();
            try {
                CBR.configure();
                CBR.preCycle();
            } catch (ExecutionException ex) {
                Logger.getLogger(CBRApplication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return CBR;
    }

    //******************************************************************/
    // Configuration
    //******************************************************************/
    @Override
    public void configure() throws ExecutionException {
        try {
            configureConnector();
            configureCaseBase();
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
    }

    /**
     * Configures the connector
     */
    @Generated(value = {"CS-PTConector"})
    private void configureConnector() throws InitializingException {
        connector = new jcolibri.connector.PlainTextConnector();
        connector.initFromXMLfile(jcolibri.util.FileIO
                .findFile("src/main/java/br/ufsm/config/plainTextConnectorConfig.xml"));
    }

    /**
     * Configures the case base
     */
    @Generated(value = {"CS-CaseManager"})
    private void configureCaseBase() throws InitializingException {
        casebase = new jcolibri.casebase.LinearCaseBase();
    }

    //******************************************************************/
    // Similarity
    //******************************************************************/
    /**
     * Configures the similarity
     */
    @Generated(value = {"CS-Similarity"})
    private NNConfig getSimilarityConfig() {
        NNConfig simConfig = new NNConfig();
        //simConfig.addMapping("PrimeiraCartaRobo",);

        return simConfig;
    }

    //******************************************************************/
    // Methods
    //******************************************************************/
    @Generated(value = {"ColibriStudio"})
    @Override
    public CBRCaseBase preCycle() throws ExecutionException {
        casebase.init(connector);
        for (CBRCase c : casebase.getCases()) {
            System.out.println(c);
        }
        return casebase;
    }

    // @Generated(value = {"ColibriStudio"})
    @Override
    public void cycle(CBRQuery query) throws ExecutionException {

    }

    public Collection<RetrievalResult> consultaCartasPrimeiraRodada(CBRQuery query) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());

        simConfig.addMapping(new Attribute("JogadorMao", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());

        simConfig.addMapping(new Attribute("CartaAltaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaMediaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaBaixaRobo", CaseDescription.class), new Interval(68));

        return aplicarSimilaridade(query, simConfig);

    }

    public Collection<RetrievalResult> consultaCartasSegundaRodada(CBRQuery query) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());

        simConfig.addMapping(new Attribute("JogadorMao", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorSegundaRodada", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("PrimeiraCartaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("PrimeiraCartaHumano", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("SegundaCartaHumano", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaAltaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaMediaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaBaixaRobo", CaseDescription.class), new Interval(68));

        return aplicarSimilaridade(query, simConfig);

    }

    public Collection<RetrievalResult> consultaCartasTerceiraRodada(CBRQuery query) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());

        simConfig.addMapping(new Attribute("JogadorMao", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorSegundaRodada", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("GanhadorTerceiraRodada", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("PrimeiraCartaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("SegundaCartaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("PrimeiraCartaHumano", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("SegundaCartaHumano", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("TerceiraCartaHumano", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaAltaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaMediaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaBaixaRobo", CaseDescription.class), new Interval(68));

        return aplicarSimilaridade(query, simConfig);

    }

    private void defineAtributosDeAcordoComRodada(NNConfig simConfig, int rodada) {
        simConfig.addMapping(new Attribute("CartaAltaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaMediaRobo", CaseDescription.class), new Interval(68));
        simConfig.addMapping(new Attribute("CartaBaixaRobo", CaseDescription.class), new Interval(68));

        switch (rodada) {
            case 1:
                simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("PrimeiraCartaHumano", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("PrimeiraCartaRobo", CaseDescription.class), new Interval(68));
                break;
            case 2:
                simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("GanhadorSegundaRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("PrimeiraCartaRobo", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("SegundaCartaRobo", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("PrimeiraCartaHumano", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("SegundaCartaHumano", CaseDescription.class), new Interval(68));
                break;
            case 3:
                simConfig.addMapping(new Attribute("GanhadorPrimeiraRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("GanhadorSegundaRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("GanhadorTerceiraRodada", CaseDescription.class), new Equal());
                simConfig.addMapping(new Attribute("PrimeiraCartaRobo", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("SegundaCartaRobo", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("TerceiraCartaRobo", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("PrimeiraCartaHumano", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("SegundaCartaHumano", CaseDescription.class), new Interval(68));
                simConfig.addMapping(new Attribute("TerceiraCartaHumano", CaseDescription.class), new Interval(68));
                break;
        }
    }

    public Collection<RetrievalResult> pedirTruco(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> pedirRetruco(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouRetruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouRetruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> pedirValeQuatro(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouValeQuatro", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouValeQuatro", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> aceitarRejeitarTruco(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorNaoQuisTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> aceitarReTruco(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouRetruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouRetruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorNaoQuisTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> aceitarValeQuatro(CBRQuery query, int rodada) {
        NNConfig simConfig = this.getSimilarityConfig();
        simConfig.setDescriptionSimFunction(new Average());
        simConfig.addMapping(new Attribute("JogadorChamouValeQuatro", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("QuandoJogadorChamouValeQuatro", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorNaoQuisTruco", CaseDescription.class), new Equal());
        simConfig.addMapping(new Attribute("JogadorGanhouTruco", CaseDescription.class), new Equal());
        this.defineAtributosDeAcordoComRodada(simConfig, rodada);
        return aplicarSimilaridade(query, simConfig);
    }

    public Collection<RetrievalResult> aplicarSimilaridade(CBRQuery query, NNConfig simConfig) {
        // A bit of verbose
        System.out.println("Query Description:");
        System.out.println(query.getDescription());
        System.out.println();

        // Execute NN
        Collection<RetrievalResult> eval = NNScoringMethod.evaluateSimilarity(this.casebase.getCases(), query, simConfig);

        // Select k cases
        eval = SelectCases.selectTopKRR(eval, 2);

        // Print the retrieval
        System.out.println("Retrieved cases:");
        for (RetrievalResult nse : eval) {
            System.out.println(nse);
        }
        return eval;
    }

    @Generated(value = {"ColibriStudio"})
    @Override
    public void postCycle() throws ExecutionException {
        connector.close();
    }
}