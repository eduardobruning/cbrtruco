/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufsm.cbrtruco.representation;

/**
 *
 * @author Eduardo Bruning
 */
import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

public class CaseDescription implements CaseComponent {


    /* Generated Class. Please Do Not Modify... */
    private String IdPartida;

    public String getIdPartida() {
        return IdPartida;
    }

    public void setIdPartida(String IdPartida0) {
        this.IdPartida = IdPartida0;
    }

    private String IdMao;

    public String getIdMao() {
        return IdMao;
    }

    public void setIdMao(String IdMao1) {
        this.IdMao = IdMao1;
    }

    private String JogadorMao;

    public String getJogadorMao() {
        return JogadorMao;
    }

    public void setJogadorMao(String JogadorMao2) {
        this.JogadorMao = JogadorMao2;
    }

    private java.lang.Integer CartaAltaRobo;

    public java.lang.Integer getCartaAltaRobo() {
        return CartaAltaRobo;
    }

    public void setCartaAltaRobo(java.lang.Integer CartaAltaRobo3) {
        this.CartaAltaRobo = CartaAltaRobo3;
    }

    private java.lang.Integer CartaMediaRobo;

    public java.lang.Integer getCartaMediaRobo() {
        return CartaMediaRobo;
    }

    public void setCartaMediaRobo(java.lang.Integer CartaMediaRobo4) {
        this.CartaMediaRobo = CartaMediaRobo4;
    }

    private java.lang.Integer CartaBaixaRobo;

    public java.lang.Integer getCartaBaixaRobo() {
        return CartaBaixaRobo;
    }

    public void setCartaBaixaRobo(java.lang.Integer CartaBaixaRobo5) {
        this.CartaBaixaRobo = CartaBaixaRobo5;
    }

    private java.lang.Integer CartaAltaHumano;

    public java.lang.Integer getCartaAltaHumano() {
        return CartaAltaHumano;
    }

    public void setCartaAltaHumano(java.lang.Integer CartaAltaHumano6) {
        this.CartaAltaHumano = CartaAltaHumano6;
    }

    private java.lang.Integer CartaMediaHumano;

    public java.lang.Integer getCartaMediaHumano() {
        return CartaMediaHumano;
    }

    public void setCartaMediaHumano(java.lang.Integer CartaMediaHumano7) {
        this.CartaMediaHumano = CartaMediaHumano7;
    }

    private java.lang.Integer CartaBaixaHumano;

    public java.lang.Integer getCartaBaixaHumano() {
        return CartaBaixaHumano;
    }

    public void setCartaBaixaHumano(java.lang.Integer CartaBaixaHumano8) {
        this.CartaBaixaHumano = CartaBaixaHumano8;
    }

    private java.lang.Integer PrimeiraCartaRobo;

    public java.lang.Integer getPrimeiraCartaRobo() {
        return PrimeiraCartaRobo;
    }

    public void setPrimeiraCartaRobo(java.lang.Integer PrimeiraCartaRobo9) {
        this.PrimeiraCartaRobo = PrimeiraCartaRobo9;
    }

    private java.lang.Integer PrimeiraCartaHumano;

    public java.lang.Integer getPrimeiraCartaHumano() {
        return PrimeiraCartaHumano;
    }

    public void setPrimeiraCartaHumano(java.lang.Integer PrimeiraCartaHumano10) {
        this.PrimeiraCartaHumano = PrimeiraCartaHumano10;
    }

    private java.lang.Integer SegundaCartaRobo;

    public java.lang.Integer getSegundaCartaRobo() {
        return SegundaCartaRobo;
    }

    public void setSegundaCartaRobo(java.lang.Integer SegundaCartaRobo11) {
        this.SegundaCartaRobo = SegundaCartaRobo11;
    }

    private java.lang.Integer SegundaCartaHumano;

    public java.lang.Integer getSegundaCartaHumano() {
        return SegundaCartaHumano;
    }

    public void setSegundaCartaHumano(java.lang.Integer SegundaCartaHumano12) {
        this.SegundaCartaHumano = SegundaCartaHumano12;
    }

    private java.lang.Integer TerceiraCartaRobo;

    public java.lang.Integer getTerceiraCartaRobo() {
        return TerceiraCartaRobo;
    }

    public void setTerceiraCartaRobo(java.lang.Integer TerceiraCartaRobo13) {
        this.TerceiraCartaRobo = TerceiraCartaRobo13;
    }

    private java.lang.Integer TerceiraCartaHumano;

    public java.lang.Integer getTerceiraCartaHumano() {
        return TerceiraCartaHumano;
    }

    public void setTerceiraCartaHumano(java.lang.Integer TerceiraCartaHumano14) {
        this.TerceiraCartaHumano = TerceiraCartaHumano14;
    }

    private String GanhadorPrimeiraRodada;

    public String getGanhadorPrimeiraRodada() {
        return GanhadorPrimeiraRodada;
    }

    public void setGanhadorPrimeiraRodada(String GanhadorPrimeiraRodada15) {
        this.GanhadorPrimeiraRodada = GanhadorPrimeiraRodada15;
    }

    private String GanhadorSegundaRodada;

    public String getGanhadorSegundaRodada() {
        return GanhadorSegundaRodada;
    }

    public void setGanhadorSegundaRodada(String GanhadorSegundaRodada16) {
        this.GanhadorSegundaRodada = GanhadorSegundaRodada16;
    }

    private String GanhadorTerceiraRodada;

    public String getGanhadorTerceiraRodada() {
        return GanhadorTerceiraRodada;
    }

    public void setGanhadorTerceiraRodada(String GanhadorTerceiraRodada17) {
        this.GanhadorTerceiraRodada = GanhadorTerceiraRodada17;
    }

    private String RoboCartaVirada;

    public String getRoboCartaVirada() {
        return RoboCartaVirada;
    }

    public void setRoboCartaVirada(String RoboCartaVirada18) {
        this.RoboCartaVirada = RoboCartaVirada18;
    }

    private String HumanoCartaVirada;

    public String getHumanoCartaVirada() {
        return HumanoCartaVirada;
    }

    public void setHumanoCartaVirada(String HumanoCartaVirada19) {
        this.HumanoCartaVirada = HumanoCartaVirada19;
    }

    private String JogadorPediuEnvido;

    public String getJogadorPediuEnvido() {
        return JogadorPediuEnvido;
    }

    public void setJogadorPediuEnvido(String JogadorPediuEnvido20) {
        this.JogadorPediuEnvido = JogadorPediuEnvido20;
    }

    private String JogadorPediuFaltaEnvido;

    public String getJogadorPediuFaltaEnvido() {
        return JogadorPediuFaltaEnvido;
    }

    public void setJogadorPediuFaltaEnvido(String JogadorPediuFaltaEnvido21) {
        this.JogadorPediuFaltaEnvido = JogadorPediuFaltaEnvido21;
    }

    private String JogadorPediuRealEnvido;

    public String getJogadorPediuRealEnvido() {
        return JogadorPediuRealEnvido;
    }

    public void setJogadorPediuRealEnvido(String JogadorPediuRealEnvido22) {
        this.JogadorPediuRealEnvido = JogadorPediuRealEnvido22;
    }

    private String PontosEnvidoRobo;

    public String getPontosEnvidoRobo() {
        return PontosEnvidoRobo;
    }

    public void setPontosEnvidoRobo(String PontosEnvidoRobo23) {
        this.PontosEnvidoRobo = PontosEnvidoRobo23;
    }

    private String PontosEnvidoHumano;

    public String getPontosEnvidoHumano() {
        return PontosEnvidoHumano;
    }

    public void setPontosEnvidoHumano(String PontosEnvidoHumano24) {
        this.PontosEnvidoHumano = PontosEnvidoHumano24;
    }

    private String JogadorNaoQuisEnvido;

    public String getJogadorNaoQuisEnvido() {
        return JogadorNaoQuisEnvido;
    }

    public void setJogadorNaoQuisEnvido(String JogadorNaoQuisEnvido25) {
        this.JogadorNaoQuisEnvido = JogadorNaoQuisEnvido25;
    }

    private String JogadorGanhouEnvido;

    public String getJogadorGanhouEnvido() {
        return JogadorGanhouEnvido;
    }

    public void setJogadorGanhouEnvido(String JogadorGanhouEnvido26) {
        this.JogadorGanhouEnvido = JogadorGanhouEnvido26;
    }

    private java.lang.Integer TotalPontosEnvido;

    public java.lang.Integer getTotalPontosEnvido() {
        return TotalPontosEnvido;
    }

    public void setTotalPontosEnvido(java.lang.Integer TotalPontosEnvido27) {
        this.TotalPontosEnvido = TotalPontosEnvido27;
    }

    private String JogadorCantouFlor;

    public String getJogadorCantouFlor() {
        return JogadorCantouFlor;
    }

    public void setJogadorCantouFlor(String JogadorCantouFlor28) {
        this.JogadorCantouFlor = JogadorCantouFlor28;
    }

    private String JogadorCantouContraFlor;

    public String getJogadorCantouContraFlor() {
        return JogadorCantouContraFlor;
    }

    public void setJogadorCantouContraFlor(String JogadorCantouContraFlor29) {
        this.JogadorCantouContraFlor = JogadorCantouContraFlor29;
    }

    private String JogadorCantouContraFlorAoResto;

    public String getJogadorCantouContraFlorAoResto() {
        return JogadorCantouContraFlorAoResto;
    }

    public void setJogadorCantouContraFlorAoResto(String JogadorCantouContraFlorAoResto30) {
        this.JogadorCantouContraFlorAoResto = JogadorCantouContraFlorAoResto30;
    }

    private String JogadorNaoQuisFlor;

    public String getJogadorNaoQuisFlor() {
        return JogadorNaoQuisFlor;
    }

    public void setJogadorNaoQuisFlor(String JogadorNaoQuisFlor31) {
        this.JogadorNaoQuisFlor = JogadorNaoQuisFlor31;
    }

    private String PontosDaFlorRobo;

    public String getPontosDaFlorRobo() {
        return PontosDaFlorRobo;
    }

    public void setPontosDaFlorRobo(String PontosDaFlorRobo32) {
        this.PontosDaFlorRobo = PontosDaFlorRobo32;
    }

    private String PontosDaFlorHumano;

    public String getPontosDaFlorHumano() {
        return PontosDaFlorHumano;
    }

    public void setPontosDaFlorHumano(String PontosDaFlorHumano33) {
        this.PontosDaFlorHumano = PontosDaFlorHumano33;
    }

    private String JogadorGanhouFlor;

    public String getJogadorGanhouFlor() {
        return JogadorGanhouFlor;
    }

    public void setJogadorGanhouFlor(String JogadorGanhouFlor34) {
        this.JogadorGanhouFlor = JogadorGanhouFlor34;
    }

    private java.lang.Integer TotalPontosViaFlor;

    public java.lang.Integer getTotalPontosViaFlor() {
        return TotalPontosViaFlor;
    }

    public void setTotalPontosViaFlor(java.lang.Integer TotalPontosViaFlor35) {
        this.TotalPontosViaFlor = TotalPontosViaFlor35;
    }

    private String JogadorNaoMostrouPontosEnvido;

    public String getJogadorNaoMostrouPontosEnvido() {
        return JogadorNaoMostrouPontosEnvido;
    }

    public void setJogadorNaoMostrouPontosEnvido(String JogadorNaoMostrouPontosEnvido36) {
        this.JogadorNaoMostrouPontosEnvido = JogadorNaoMostrouPontosEnvido36;
    }

    private String JogadorNaoMostrouPontosFlor;

    public String getJogadorNaoMostrouPontosFlor() {
        return JogadorNaoMostrouPontosFlor;
    }

    public void setJogadorNaoMostrouPontosFlor(String JogadorNaoMostrouPontosFlor37) {
        this.JogadorNaoMostrouPontosFlor = JogadorNaoMostrouPontosFlor37;
    }

    private String JogadorChamouTruco;

    public String getJogadorChamouTruco() {
        return JogadorChamouTruco;
    }

    public void setJogadorChamouTruco(String JogadorChamouTruco38) {
        this.JogadorChamouTruco = JogadorChamouTruco38;
    }

    private String QuandoJogadorChamouTruco;

    public String getQuandoJogadorChamouTruco() {
        return QuandoJogadorChamouTruco;
    }

    public void setQuandoJogadorChamouTruco(String QuandoJogadorChamouTruco39) {
        this.QuandoJogadorChamouTruco = QuandoJogadorChamouTruco39;
    }

    private String JogadorChamouRetruco;

    public String getJogadorChamouRetruco() {
        return JogadorChamouRetruco;
    }

    public void setJogadorChamouRetruco(String JogadorChamouRetruco40) {
        this.JogadorChamouRetruco = JogadorChamouRetruco40;
    }

    private String QuandoJogadorChamouRetruco;

    public String getQuandoJogadorChamouRetruco() {
        return QuandoJogadorChamouRetruco;
    }

    public void setQuandoJogadorChamouRetruco(String QuandoJogadorChamouRetruco41) {
        this.QuandoJogadorChamouRetruco = QuandoJogadorChamouRetruco41;
    }

    private String JogadorChamouValeQuatro;

    public String getJogadorChamouValeQuatro() {
        return JogadorChamouValeQuatro;
    }

    public void setJogadorChamouValeQuatro(String JogadorChamouValeQuatro42) {
        this.JogadorChamouValeQuatro = JogadorChamouValeQuatro42;
    }

    private String QuandoJogadorChamouValeQuatro;

    public String getQuandoJogadorChamouValeQuatro() {
        return QuandoJogadorChamouValeQuatro;
    }

    public void setQuandoJogadorChamouValeQuatro(String QuandoJogadorChamouValeQuatro43) {
        this.QuandoJogadorChamouValeQuatro = QuandoJogadorChamouValeQuatro43;
    }

    private String JogadorNaoQuisTruco;

    public String getJogadorNaoQuisTruco() {
        return JogadorNaoQuisTruco;
    }

    public void setJogadorNaoQuisTruco(String JogadorNaoQuisTruco44) {
        this.JogadorNaoQuisTruco = JogadorNaoQuisTruco44;
    }

    private String JogadorGanhouTruco;

    public String getJogadorGanhouTruco() {
        return JogadorGanhouTruco;
    }

    public void setJogadorGanhouTruco(String JogadorGanhouTruco45) {
        this.JogadorGanhouTruco = JogadorGanhouTruco45;
    }

    private java.lang.Integer TotalPontosTruco;

    public java.lang.Integer getTotalPontosTruco() {
        return TotalPontosTruco;
    }

    public void setTotalPontosTruco(java.lang.Integer TotalPontosTruco46) {
        this.TotalPontosTruco = TotalPontosTruco46;
    }

    private java.lang.Integer TotalPontosRoboAnteriorMao;

    public java.lang.Integer getTotalPontosRoboAnteriorMao() {
        return TotalPontosRoboAnteriorMao;
    }

    public void setTotalPontosRoboAnteriorMao(java.lang.Integer TotalPontosRoboAnteriorMao47) {
        this.TotalPontosRoboAnteriorMao = TotalPontosRoboAnteriorMao47;
    }

    private java.lang.Integer TotalPontosHumanoAnteriorMao;

    public java.lang.Integer getTotalPontosHumanoAnteriorMao() {
        return TotalPontosHumanoAnteriorMao;
    }

    public void setTotalPontosHumanoAnteriorMao(java.lang.Integer TotalPontosHumanoAnteriorMao48) {
        this.TotalPontosHumanoAnteriorMao = TotalPontosHumanoAnteriorMao48;
    }

    private java.lang.Integer TotalPontosRoboPosteriorMao;

    public java.lang.Integer getTotalPontosRoboPosteriorMao() {
        return TotalPontosRoboPosteriorMao;
    }

    public void setTotalPontosRoboPosteriorMao(java.lang.Integer TotalPontosRoboPosteriorMao49) {
        this.TotalPontosRoboPosteriorMao = TotalPontosRoboPosteriorMao49;
    }

    private java.lang.Integer TotalPontosHumanoPosteriorMao;

    public java.lang.Integer getTotalPontosHumanoPosteriorMao() {
        return TotalPontosHumanoPosteriorMao;
    }

    public void setTotalPontosHumanoPosteriorMao(java.lang.Integer TotalPontosHumanoPosteriorMao50) {
        this.TotalPontosHumanoPosteriorMao = TotalPontosHumanoPosteriorMao50;
    }

    private String JogadorRoboMentiuEnvido;

    public String getJogadorRoboMentiuEnvido() {
        return JogadorRoboMentiuEnvido;
    }

    public void setJogadorRoboMentiuEnvido(String JogadorRoboMentiuEnvido51) {
        this.JogadorRoboMentiuEnvido = JogadorRoboMentiuEnvido51;
    }

    private String JogadorHumanoMentiuEnvido;

    public String getJogadorHumanoMentiuEnvido() {
        return JogadorHumanoMentiuEnvido;
    }

    public void setJogadorHumanoMentiuEnvido(String JogadorHumanoMentiuEnvido52) {
        this.JogadorHumanoMentiuEnvido = JogadorHumanoMentiuEnvido52;
    }

    private String JogadorRoboMentiuFlor;

    public String getJogadorRoboMentiuFlor() {
        return JogadorRoboMentiuFlor;
    }

    public void setJogadorRoboMentiuFlor(String JogadorRoboMentiuFlor53) {
        this.JogadorRoboMentiuFlor = JogadorRoboMentiuFlor53;
    }

    private String JogadorHumanoMentiuFlor;

    public String getJogadorHumanoMentiuFlor() {
        return JogadorHumanoMentiuFlor;
    }

    public void setJogadorHumanoMentiuFlor(String JogadorHumanoMentiuFlor54) {
        this.JogadorHumanoMentiuFlor = JogadorHumanoMentiuFlor54;
    }

    private String JogadorRoboMentiuTruco;

    public String getJogadorRoboMentiuTruco() {
        return JogadorRoboMentiuTruco;
    }

    public void setJogadorRoboMentiuTruco(String JogadorRoboMentiuTruco55) {
        this.JogadorRoboMentiuTruco = JogadorRoboMentiuTruco55;
    }

    private String JogadorHumanoMentiuTruco;

    public String getJogadorHumanoMentiuTruco() {
        return JogadorHumanoMentiuTruco;
    }

    public void setJogadorHumanoMentiuTruco(String JogadorHumanoMentiuTruco56) {
        this.JogadorHumanoMentiuTruco = JogadorHumanoMentiuTruco56;
    }

    private String JogadorFoiBaralho;

    public String getJogadorFoiBaralho() {
        return JogadorFoiBaralho;
    }

    public void setJogadorFoiBaralho(String JogadorFoiBaralho57) {
        this.JogadorFoiBaralho = JogadorFoiBaralho57;
    }

    private java.lang.Integer Id;

    public java.lang.Integer getId() {
        return Id;
    }

    public void setId(java.lang.Integer Id58) {
        this.Id = Id58;
    }

    @Override
    public Attribute getIdAttribute() {
        return new Attribute("Id", this.getClass());
    }

    public String toString() {
        return "["
                + " Cartas Robo: "
                + " , " + CartaBaixaRobo
                + " , " + CartaMediaRobo
                + " , " + CartaAltaRobo
                + " , 1°" + PrimeiraCartaRobo
                + " , 2°" + SegundaCartaRobo
                + " , 3°" + TerceiraCartaRobo
                + "     Jogador Mão: "
                + " , " + JogadorMao
                + "     Flor: "
                + " , " + JogadorGanhouFlor
                + " , " + TotalPontosViaFlor
                + " , " + JogadorCantouFlor
                + " , " + JogadorNaoQuisFlor
                + " , " + JogadorCantouContraFlor
                + " , " + PontosDaFlorRobo
                + " , " + PontosDaFlorHumano
                + " , " + JogadorHumanoMentiuFlor
                + " , " + JogadorCantouContraFlorAoResto
                + " , " + JogadorRoboMentiuFlor
                + " , " + JogadorNaoMostrouPontosFlor
                + "     Truco: "
                + " , " + JogadorHumanoMentiuTruco
                + " , " + JogadorRoboMentiuTruco
                + " , " + JogadorNaoQuisTruco
                + " , " + JogadorGanhouTruco
                + " , " + JogadorChamouTruco
                + " , " + QuandoJogadorChamouRetruco
                + " , " + QuandoJogadorChamouValeQuatro
                + " , " + JogadorChamouRetruco
                + " , " + JogadorChamouValeQuatro
                + " , " + QuandoJogadorChamouTruco
                + " , " + TotalPontosTruco
                + "     Envido: "
                + " , " + JogadorGanhouEnvido
                + " , " + TotalPontosEnvido
                + " , " + JogadorPediuEnvido
                + " , " + JogadorNaoQuisEnvido
                + " , " + JogadorHumanoMentiuEnvido
                + " , " + PontosEnvidoRobo
                + " , " + JogadorNaoMostrouPontosEnvido
                + " , " + JogadorPediuRealEnvido
                + " , " + PontosEnvidoHumano
                + " , " + JogadorPediuFaltaEnvido
                + " , " + JogadorRoboMentiuEnvido
                + "     Total Pontos: "
                + " , " + TotalPontosRoboAnteriorMao
                + " , " + TotalPontosHumanoAnteriorMao
                + " , " + TotalPontosRoboPosteriorMao
                + " , " + TotalPontosHumanoPosteriorMao
                + "     Carta Virada e ir baralho: "
                + " , " + RoboCartaVirada
                + " , " + HumanoCartaVirada
                + " , " + JogadorFoiBaralho
                + "     IDS: "
                + " , " + Id
                + " , " + IdPartida
                + " , " + IdMao
                + "     Ganhador Rodadas: "
                + " , " + GanhadorPrimeiraRodada
                + " , " + GanhadorSegundaRodada
                + " , " + GanhadorTerceiraRodada
                + "     Cartas Humano: "
                + " , " + PrimeiraCartaHumano
                + " , " + SegundaCartaHumano
                + " , " + TerceiraCartaHumano
                + " , " + CartaBaixaHumano
                + " , " + CartaMediaHumano
                + " , " + CartaAltaHumano
                + "]";
    }

}
